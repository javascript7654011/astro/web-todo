import { defineConfig } from "astro/config"

// https://astro.build/config
export default defineConfig({
  server: {
    host: "localhost",
    port: 4200,
  },
  imports: {
    "@/": "src",
  },
})
